<?php

namespace App\Controllers;

use App\Models\UserModel;

class Task extends BaseController
{
    public function index()
    {
        $data = [];
        helper(['form']);


        if ($this->request->getMethod() == 'post') {
            //validation here
            $rules = [
               
                'email' => 'required|min_length[6]|max_length[50]|valid_email',
                'password' => 'required|min_length[8]|max_length[225]|validateUser[email,password]',
                
            ];

            $errors = [
                'password' => [
                    'validateUser' => 'Email or Password don\'t match'
                ]
            ];

            if (! $this->validate($rules, $errors)) {
                $data['validation'] = $this->validator;
            }else{
                //store the user in database
                $model = new UserModel();

                $user = $model->where('email', $this->request->getVar('email'))
                              ->first();

               $this->setUserSession($user);
              
                //$session->setFlashdata('success', 'Succesful Sign Up');
                return redirect()->to('dashboard');
            }
        }
        
        echo view('components/header', $data);
        echo view('login');
        echo view('components/footer');
        
    }

    private function setUserSession($user){
        $data = [
            'id' => $user['id'],
            'firstname' => $user['firstname'],
            'lastname' => $user['lastname'],
            'email' => $user['email'],
            'isLoggedIn' => true,
        ];

        session()->set($data);
        return true;
    }

    public function signup()
    {
        $data = [];
        helper(['form']);

        if ($this->request->getMethod() == 'post') {
            //validation here
            $rules = [
                'firstname' => 'required|min_length[3]|max_length[20]',
                'lastname' => 'required|min_length[3]|max_length[20]',
                'email' => 'required|min_length[6]|max_length[50]|valid_email|is_unique[users.email]',
                'password' => 'required|min_length[8]|max_length[225]',
                'cpassword' => 'matches[password]',
            ];

            if (! $this->validate($rules)) {
                $data['validation'] = $this->validator;
            }else{
                //store the user in database
                $model = new UserModel();

                $newData = [
                    'firstname' => $this->request->getVar('firstname'),
                    'lastname' => $this->request->getVar('lastname'),
                    'email' => $this->request->getVar('email'),
                    'password' => $this->request->getVar('password'),
                ];
                $model->save($newData);
                $session = session();
                $session->setFlashdata('success', 'Succesful Sign Up');
                return redirect()->to('/');
            }
        }

        echo view('components/header', $data);
        echo view('signup');
        echo view('components/footer');
        
    }

    public function profile(){
        // if(!session()->get('isLoggedIn'))
        //     redirect()->to('/');

        $data = [];
        helper(['form']);
        $model = new UserModel();

        if ($this->request->getMethod() == 'post') {
            //validation here
            $rules = [
                'firstname' => 'required|min_length[3]|max_length[20]',
                'lastname' => 'required|min_length[3]|max_length[20]',
                'email' => 'required|min_length[6]|max_length[50]|valid_email|',
            ];


            if($this->request->getPost('password') != ''){
                $rules['password'] = 'required|min_length[8]|max_length[255]';
                $rules['cpassword'] = 'matches[password]';
            }

            if (! $this->validate($rules)) {
                $data['validation'] = $this->validator;
            }else{
                
                

                $newData = [
                    'id' => session()->get('id'),
                    'firstname' => $this->request->getPost('firstname'),
                    'lastname' => $this->request->getPost('lastname'),
                    'email' => $this->request->getPost('email'),
                ];
                if($this->request->getPost('password') != ''){

                    $newData['password'] = $this->request->getPost('password');
                }
                $model->save($newData);
                
                session()->setFlashdata('success', 'Succesfully Updated');
                return redirect()->to('/profile');
            }
        }

        $data['user'] = $model->where('id', session()->get('id'))->first();
        echo view('components/header', $data);
        echo view('profile');
        echo view('components/footer');

    }
    
    public function logout(){
        session()->destroy();
        return redirect()->to('/');
    }


}

