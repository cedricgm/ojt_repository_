<div class="form-bg">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm8- offset-sm-2 col-md-6 offset-md-3 mt-5 pt-3 pb-3">
                <div class="form-container">
                    <form class="form-horizontal" action="/" method="post">
                        <h3 class="title">Good Day!</h3>
                        <span class="description">How are you doing?</span>
                            <?php if (session()->get('success')): ?>
                                <div class="alert alert-success" role="alert">
                                    <?= session()->get('success') ?>
                                </div>
                            <?php endif; ?>


                                <!-- <form class="" action="/" method="post"> -->
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="email" id="email" placeholder="Enter your email" value="<?= set_value('email') ?>"> 
                                    </div>
                                    
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="password" id="password" placeholder="Enter your password" value="">
                                    </div>
                                    <?php if (isset($validation)): ?>
                                            <div class="col-12">
                                                <div class="alert alert-danger" role="alert">
                                                    <?= $validation->listErrors() ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    <div class="row">
                                        <div class="col-12 col-sm-4">
                                            <button class="btn signin">LOGIN</button>
                                        </div>
                                        
                                    <div class="col-12 col-sm-8 text-right" style="margin-top: 10px;">
                                        <a href="/signup">No account yet? Sign up now.</a>
                                    </div>

                    </form>
                </div>
            </div>     
        </div>
    </div>   
</div>




