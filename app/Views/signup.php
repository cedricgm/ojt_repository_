

<div class="container">
    <div class="row">
        <div class="col-12 col-sm8- offset-sm-2 col-md-6 offset-md-3 mt-5 pt-3 pb-3 bg-white form-wrapper">
            <div class="container">
            <h2>Register Here.</h2>
            <hr>
            
            <form class="form-horizontal" action="/signup" method="post">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label for="firstname">First Name</label>
                            <input type="text" class="form-control" name="firstname" id="firstname" placeholder="Enter your first name" value="<?= set_value('firstname') ?>">
                        </div>
                    </div>

                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="lastname">Last Name</label>
                            <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Enter your last name" value="<?= set_value('lastname') ?>">
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                        <label for="email">Email</label>
                            <input type="text" class="form-control" name="email" id="email" placeholder="Enter your email" value="<?= set_value('email') ?>"> 
                        </div>
                    </div>
                    <br>

                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter your password" value="">
                        </div>
                    </div>
                    <br>

                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                        <label for="cpassword">Confirm Password</label>
                            <input type="password" class="form-control" name="cpassword" id="cpassword" placeholder="Confirm your password" value="">
                        </div>
                    </div>
                    <?php if (isset($validation)): ?>
                        <div class="col-12">
                            <div class="alert alert-danger" role="alert">
                                <?= $validation->listErrors() ?>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-primary sigbot" type="submit">SIGNUP</button>
                    </div>
                    <br>
                    
                    <div class="col-12 col-sm-8 text-right">
                        <a href="/">Have an account already? Login now.</a>
                     </div>
                </div>
            </form> 
            </div>    
        </div>
    </div>   
</div>




